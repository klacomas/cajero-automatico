package Main;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import modelo.Vendedor;

public class MainClass {

	public static void main(String[] args) {
		
		
		
		

		
		Scanner sc = new Scanner(System.in);
		boolean salir = false;
		String opcion = "salir";
		while(!opcion.equals("9")) {
		
			System.out.println("1--Crear evento\n");
			System.out.println("2--Vender entrada\n");
			System.out.println("3--Ingresar a evento\n");
			System.out.println("4--Cambio de estado\n");
			System.out.println("5--Entradas vendidas por vendedor\n"); //listo
			System.out.println("9--salir");
			
			opcion = sc.nextLine();
		
			if(opcion.equals("1")) {
				System.out.printf("Crear evento seleccionado\n");
				crearEvento();
			}
			else if(opcion.equals("2")) {
				System.out.printf("Vender entrada seleccionado\n");
				venderEntrada();
			}
			else if(opcion.equals("3")) {
				System.out.printf("Ingreso a evento\n");
				ingresoDeEvento();
			}
			else if(opcion.equals("4")) {
				System.out.printf("Cambio de estado seleccionado\n");
				cambioDeEstado();
			}
			else if(opcion.equals("5")) {
				System.out.printf("Entradas vendidas por vendedor\n");
				entradasVendidas();
			}
			else if(opcion.equals("9")) {
				System.out.printf("Salir\n");
			}
			else  {
				System.out.printf("numero no ingresado correctamente\n");
			}

		}
	}
	

	private static void entradasVendidas() {
		
		int vendidasENormales = 0;
		int vendidasEVip = 0;
		String nombreDelVendedor = null;
		
		//este metodo es para calcular la cantidad de entradas vendidas por el vendedor
		System.out.println("---Calculo entrada vendida por vendedor---");
		System.out.println("Ingrese rut del vendedor");
		Scanner sc = new Scanner (System.in); //se declara una variable sc de tipo scanner y se instancia con la clase scanner
		String rutVendedor ;        // se declara la variable rutvendedor del tipo string
		rutVendedor = sc.nextLine(); //se asigna el valor ingresado por teclado a la variable rutvendedor
		
		ArrayList<Vendedor>lstVendedores = new ArrayList<Vendedor>();
		lstVendedores = obtenerListaVendedores();

		for (Vendedor vendedortmp :lstVendedores) {
						
			if (rutVendedor.equals(vendedortmp.getRut())) {
				 vendidasENormales = vendedortmp.getVendidasENormales();
				
				 vendidasEVip = vendedortmp.getVendidasEVip();
				
				 nombreDelVendedor = vendedortmp.getNombre(); //se crea la variable nombre del vendedor y se muestra
				
				
				break;
			}
			
		}
		System.out.println("las entradas vendidas por el vendedor :" + nombreDelVendedor  + " son : " +
		vendidasENormales + " entradas normmales y "+ vendidasEVip + " entradas vip" );
		
	}

	private static ArrayList<Vendedor> obtenerListaVendedores() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		
		ArrayList<Vendedor> listaVendedores = new ArrayList <Vendedor> ();
		//instanciar objeto del tipo vendedor 
		//darle valores a los atributos del objeto del tipo vendedor 
		//agregar el objeto recien creado a la arrayList
		//retornar el arrayList
//public Vendedor(String rut, String nombre, Date fechaNacimiento, int vendidasENormales, int vendidasEVip)		
	
		
		
		Vendedor vendedor = null ;
		
		
		 try {
		 vendedor = new Vendedor("19" , "luis" , formatter.parse("12-12-2000") , 20 , 30);
		 listaVendedores.add (vendedor);
		 vendedor = new Vendedor("20" , "juan" , formatter.parse("11-11-2011") , 21 , 32);
		 listaVendedores.add (vendedor);
		 vendedor = new Vendedor("21" , "pepe" , formatter.parse("12-11-2013") , 21 , 32);
		 listaVendedores.add (vendedor);
	
		}	catch (ParseException e) {
			e.printStackTrace();
		}

		return listaVendedores;
	}
	

	private static void cambioDeEstado() {
		//cambiar estado del evento si esta abierto o cerrado
	}

	private static void ingresoDeEvento() {
		//valida el ingresadas al evento/usos de entradas
	}


	private static void venderEntrada() {
		//se obtiene los datos del usuario y si este es vip o normal
		
		
	}

	private static void crearEvento() {
		//crear evento
		
		String nombreDelEvento  ;
		int edadMinima  ;
		int canTicket ;
		int  precioTicket;
		
		
		System.out.println("---Crear evento---");
		System.out.println("Nombre del evento");
		
		Scanner sc = new Scanner (System.in);
		nombreDelEvento = sc.nextLine();
		
		System.out.println("Edad");
		
		edadMinima = sc.nextInt();
		
		System.out.println("Ingresar cantidad de ticket");
		canTicket = sc.nextInt ();
		
		System.out.println("precio del ticket");
		 precioTicket = sc.nextInt ();
		
	}
}
