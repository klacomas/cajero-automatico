package modelo;

import java.util.Date;

public class Vendedor extends Persona {
	public Vendedor(String rut, String nombre, Date fechaNacimiento) {
		super(rut, nombre, fechaNacimiento);
		// TODO Auto-generated constructor stub
	}
	protected int vendidasENormales ; 
	protected int vendidasEVip ;
	public int getVendidasENormales() {
		return vendidasENormales;
	}
	public void setVendidasENormales(int vendidasENormales) {
		this.vendidasENormales = vendidasENormales;
	}
	public int getVendidasEVip() {
		return vendidasEVip;
	}
	public void setVendidasEVip(int vendidasEVip) {
		this.vendidasEVip = vendidasEVip;
	}
	public Vendedor(String rut, String nombre, Date fechaNacimiento, int vendidasENormales, int vendidasEVip) {
		super(rut, nombre, fechaNacimiento);
		this.vendidasENormales = vendidasENormales;
		this.vendidasEVip = vendidasEVip;
	}
	

	
	
		

	
	

}
