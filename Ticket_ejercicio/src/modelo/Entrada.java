package modelo;
import java.text.SimpleDateFormat;
public class Entrada {
    int nroAsiento;
    int precio;
    String nroCliente;
    String nroVendedor;
    boolean esVendida;
    boolean esUsada;
    public Entrada(int nroAsiento, int precio, String nroCliente, String nroVendedor, boolean esVendida,
            boolean esUsada) {
        super();
        this.nroAsiento = nroAsiento;
        this.precio = precio;
        this.nroCliente = nroCliente;
        this.nroVendedor = nroVendedor;
        this.esVendida = esVendida;
        this.esUsada = esUsada;
    }
    public int getNroAsiento() {
        return nroAsiento;
    }
    public void setNroAsiento(int nroAsiento) {
        this.nroAsiento = nroAsiento;
    }
    public int getPrecio() {
        return precio;
    }
    public void setPrecio(int precio) {
        this.precio = precio;
    }
    public String getNroCliente() {
        return nroCliente;
    }
    public void setNroCliente(String nroCliente) {
        this.nroCliente = nroCliente;
    }
    public String getNroVendedor() {
        return nroVendedor;
    }
    public void setNroVendedor(String nroVendedor) {
        this.nroVendedor = nroVendedor;
    }
    public boolean isEsVendida() {
        return esVendida;
    }
    public void setEsVendida(boolean esVendida) {
        this.esVendida = esVendida;
    }
    public boolean isEsUsada() {
        return esUsada;
    }
    public void setEsUsada(boolean esUsada) {
        this.esUsada = esUsada;
    }
    
    
    
}