package modelo;
import java.util.Scanner;
import java.util.ArrayList;
import modelo.EntradaVip;

public class Evento {
	//DECLARACION DE LAS VARIABLES PRIVADAS
	protected String nombreEvento;
	protected int edadMinima;
	protected int cantTicket;
	

	protected ArrayList<EntradaVip> listEntrada;
	protected Boolean enCurso;
	protected int nroUsadas;
	protected int nroVendidas;
	
	//constructor
	
	public Evento(String nombreEvento, int edadMinima, int cantTicket, ArrayList<EntradaVip> listEntrada,
			Boolean enCurso, int nroUsadas, int nroVendidas) {
		super();
		this.nombreEvento = nombreEvento;
		this.edadMinima = edadMinima;
		this.cantTicket = cantTicket;
		this.listEntrada = listEntrada;
		this.enCurso = enCurso;
		this.nroUsadas = nroUsadas;
		this.nroVendidas = nroVendidas;
	}
	
	//CREACION DE LOS SETTERS Y GETTERS PARA CADA VARIABLE PARA QUE PUEDAN SER LLAMADOS DESDE OTRA CLASE
	//CONJUNTO DEL NOMBRE DEL EVENTO
	public String getNombreEvento() {
		return nombreEvento;
	}
	
	public void setNombreEvento(String nombreEvento) {
		this.nombreEvento = nombreEvento;
		
	}

	//CONJUNTO PARA LA EDAD MINIMA REQUERIDA DEL EVENTO
	public int getEdadMinima() {
		return edadMinima;
	}
	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}

	public ArrayList<EntradaVip> getListEntrada() {
		return listEntrada;
	}

	public void setListEntrada(ArrayList<EntradaVip> listEntrada) {
		this.listEntrada = listEntrada;
	}

	public Boolean getEnCurso() {
		return enCurso;
	}

	public void setEnCurso(Boolean enCurso) {
		this.enCurso = enCurso;
	}

	public Integer getNroUsadas() {
		return nroUsadas;
	}

	public void setNroUsadas(Integer nroUsadas) {
		this.nroUsadas = nroUsadas;
	}

	public Integer getNroVendidas() {
		return nroVendidas;
	}

	public void setNroVendidas(Integer nroVendidas) {
		this.nroVendidas = nroVendidas;
	}
	
	public int getCantTicket() {
		return cantTicket;
	}

	public void setCantTicket(int cantTicket) {
		this.cantTicket = cantTicket;
	}

	public void crearEvento (String nombre, int edadMinima, int cantTicket, boolean enCurso) {//no hay devolución datos, solo modifica variaables
		setNombreEvento(nombre); //modificando el dato
		setEdadMinima(edadMinima);
		setCantTicket(cantTicket);
		setEnCurso(enCurso);
		setNroUsadas(0);
		setNroVendidas(0);
		int cantEntrada = getCantTicket();
		
	//van a llamarlo desde el menu
	}
	@SuppressWarnings("null")
	public ArrayList<EntradaVip> crearListaEntrada (int cantTicket, int precio) {//retorna cantTicket, precio, devuelve el aarray
		
		ArrayList<EntradaVip> entradasEvento = new ArrayList<>();
		EntradaVip entrada = null; 
		int i;
		for(i=0;i<cantTicket;i++) {
			entrada.setNroAsiento(i+1);
			entrada.setPrecio(precio);
			entrada.setNroCliente(null);
			entrada.setNroVendedor(null);
			entrada.setEsVendida(false);
			entrada.setEsUsada(false);
			//ingresa entrada			
			entradasEvento.add(entrada);
		//	System.out.println(entrada);
		}
		return entradasEvento;
	}

	public void cambiarEstado(boolean enCurso) {
		setEnCurso(enCurso);	
	}
	
	public void contarEntradas(int nroVendidas, int nroUsadas) {
		setNroVendidas(nroVendidas);
		setNroUsadas(nroUsadas);
	}

}
